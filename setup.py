from setuptools import setup, find_packages

setup(
    name='imtotxt',
    version='0.1.0',
    install_requires=[
        'pandas==1.4.3',
        'numpy>=1.23.2',
        'matplotlib>=3.5.3',
        'scipy>=1.9.0'
    ]
)
