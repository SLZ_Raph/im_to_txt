"""
im_to_txt
Transforms an image into a ASCII equivalent
1:1 resolution

Raphael Salazar 2023
"""


import numpy as np
from scipy.interpolate import RegularGridInterpolator as RGI
from matplotlib import pyplot as plt
from PIL import Image
import os
import re
import sys

def findfiles(path):
    expr0 = re.compile('.png')
    filelist = sorted(os.listdir(path))
    filelist = np.array(filelist)
    filelist = list(filelist[[bool(expr0.search(i)) for i in filelist]])

    return filelist

def list_to_str(array):
    string = ''
    for i in array:
        string+=i

    return string

def to_string(im, colorscale):
    grayscale_im = np.mean(im, axis = 2)
    grayscale_im = np.flip(grayscale_im,0)
    grayscale_im = (len(colorscale)-1)*grayscale_im/np.max(grayscale_im)

    #aspect_ratio = grayscale_im.shape[1]/grayscale_im.shape[0]
    aspect_ratio = 0.4

    # x = np.linspace(0,1,grayscale_im.shape[0])
    # y = np.linspace(0,1,grayscale_im.shape[1])
    # interpolant = RGI([x,y], grayscale_im)
    # N = 24
    # new_x = np.linspace(0,1,N)
    # new_y = np.linspace(0,1,int(30*aspect_ratio))
    # new_grayscale = np.array([interpolant((j, new_x)) for j in new_y])
    # plt.pcolormesh(new_grayscale)
    # plt.show()

    new_grayscale = np.floor(grayscale_im).astype(int)
    txt_im = [[colorscale[new_grayscale[i,j]]  for i in range(new_grayscale.shape[0])] for j in range(new_grayscale.shape[1])]
    txt_im = np.flip(np.array(txt_im).T, axis = 0)
    txt_im = [list_to_str(i) for i in txt_im]

    return txt_im


colorscales = {'basic':"█▓▒░#@8&%±+•*;:,. ",
               'basic_r':" .,:;*•+±%&8@#░▒▓█",
               '-r':"███████▓▓▓▒▒▒▒░░░**;;::,.... ",
               '':" ....,::;;**░░░▒▒▒▒▓▓▓███████"}

params = {'path':'./', 'color':''}
for i in range(1,len(sys.argv)) :
    keys = list(params.keys())
    params[keys[i-1]] = sys.argv[i]

if params['path'][-1] != '/':
    params['path']+='/'

filenames = findfiles(params['path'])
for name in filenames:
    im = np.array(Image.open(params['path']+name))
    txt = to_string(im, colorscales[params['color']])
    save_name = name.replace('png','txt')
    file = open(params['path']+save_name,'w+')
    for line in txt:
        file.write(line+'\n')
    file.close()
