# im_to_txt

## Name
im_to_txt

## Description

### Introduction
Creating little text pics with im_to_txt

### Purpose

Smol txt pics


## Installation
You can install the package easily with setup.py.
It is recommanded to use a virtual environment.

```console
python -m venv /path/to/new/virtual/environment
source /path/to/new/virtual/environment/bin/activate
cd /path/to/new/virtual/environment/
git clone git@gitlab.com:SLZ_Raph/im_to_txt.git
pip install -e .
```

## Usage
You can test the software running the file test.sh:
```console
./test.sh
```
This should output several text versions of the picture in the ./examples folder.
![Test image](./examples/logo.png)
```console
.....               
.░▒░.       ...     
..▒....     .░......
 .▒..░....  .▒░.▒▒▒.
..▒..▒▒▒░.. .▒..▒:▒.
.░▒░.░.░.*. .▒▒▒▒▒▒.
........... ........
   ...          ... 
  ..░.. .........░..
  .░█░...*..*.*.░█░.
  ..▒...░,░..░...▒..
   .▒...▒▒▒..░.░▒▒..
   .▒▒▒▒▒...*.*..▒▒▒
   ......░░.........
        ....        
```

The general syntax is as follows:
```console
python imtotxt.py <folder name> <colorbar>
```
The colorbar item is one of the predefined in the imtotxt.py file (basic, basic_r, -r).
If no colorbar is given a default one is used.
The program will transform every pictures in the folder into text.

## Authors and acknowledgment
Raphael Salazar
